#![feature(test)]

extern crate test;

fn main() {
}

fn naive_fib(n: u32) -> u32 {
    match n {
        0 => n,
        1 => n,
        _ => naive_fib(n - 1) + naive_fib(n - 2)
    }
}

fn memoisation_fib(n: usize) -> usize {
    match n {
        0 => n,
        1 => n,
        _ => {
            let mut fib: Vec<usize> = vec![0, 1];

            for i in 2..(n + 1) {
                let a = fib[i - 1];
                let b = fib[i - 2];
                fib.push(a + b);
            }

            fib[n]
        }
    }
}

fn arr_fib(n: usize) -> usize {
    match n {
        0 => n,
        1 => n,
        _ => {
            let mut fib: [usize; 2] = [0; 2];
            fib[0] = 0;
            fib[1] = 1;
            let mut res = 0;

            for i in 2..(n + 1) {
                let a = fib[0];
                let b = fib[1];

                res = a + b;
                fib[0] = b;
                fib[1] = res;
            }

            res
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn naivefib10() {
        assert_eq!(55, naive_fib(10));
    }

    #[test]
    fn memoisationfib10() {
        assert_eq!(55, memoisation_fib(10));
    }

    #[test]
    fn arrfib10() {
        assert_eq!(55, arr_fib(10));
    }

    #[bench]
    fn bench_memoisation_fib40(b: &mut Bencher) {
        b.iter(|| memoisation_fib(40));
    }

    #[bench]
    fn bench_arr_fib40(b: &mut Bencher) {
        b.iter(|| arr_fib(40));
    }

    #[bench]
    fn bench_naive_fib40(b: &mut Bencher) {
        b.iter(|| naive_fib(40));
    }
}
