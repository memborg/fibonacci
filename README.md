Implemented af naïve recursive fibonacci function and then reimplemented an optimized version based on memoisation. And I've fiddled with some tests too.

I've found my inspiration in this article [Optimising the Fibonacci sequence generating algorithm](https://codeburst.io/optimising-the-fibonacci-sequence-generating-algorithm-171ea1492772) which is based on Python.